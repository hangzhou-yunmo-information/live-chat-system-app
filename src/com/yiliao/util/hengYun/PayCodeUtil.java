package com.yiliao.util.hengYun;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import com.yiliao.util.HttpClientUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.yiliao.util.HttpUtil;
import com.yiliao.util.Md5Util;
import com.yiliao.util.SystemConfig;

import net.sf.json.JSONObject;


public class PayCodeUtil {

	private static Logger logger = LogManager.getLogger(PayCodeUtil.class);

	public static String UID2 = "";
	//商户Id
	public static String MID2 = "";
//	public static String NOTIFY_URL = "http://47.89.12.190/pay/alipayH5PayNotify.html";
//	public static String NOTIFY_URL = "http://r203966a62.iok.la/pay/alipayH5PayNotify.html";
	
//	public static String RETURN_URL = "http://47.89.12.190/success.jsp";
//	public static String RETURN_URL = "http://r203966a62.iok.la/success.jsp";
	
	public static String BASE_URL = "https://pay.paysapi.com";

	public static String PAY_URL = "https://pay.bearsoftware.net.cn/";
	
	//密钥
	public static String TOKEN2 = "mhcm5xolze9dog4ilv1pcymt15gmjyvb";

	
	/**
	 * 支付宝h5支付
	 * 
	 * @return
	 */
	public static Map<String, Object> payAliPayH5(String url,String orderNo, BigDecimal amount, String body,String MID,String TOKEN,String pay_type) {
		
//		String url = "https://pay.iisck.cn/gateway/h5pay2";
//		String url2 = "https://pay.iisck.cn/gateway/pay";
//		switch (pay_type) {
//		case "101":
//			url=url2;
//			break;
//		}
		StringBuffer sign = new StringBuffer();
		sign.append("version=").append("1.0"); // 版本
		sign.append("&mch_id=").append(MID); // 商户号
		sign.append("&pay_type=").append(pay_type); // 类型
		sign.append("&total_amount=").append(amount.multiply(new BigDecimal(100)).intValue()); // 金额 单位(分)
		sign.append("&out_trade_no=").append(orderNo); // 订单号
		sign.append("&notify_url=").append(SystemConfig.getValue("h5NotifyUrl")); // 回调地址
		String sign_m = Md5Util.encodeByMD5ByLow(sign.toString() + "&key="+TOKEN); // sign
		sign.append("&return_url=").append(SystemConfig.getValue("h5ReturnUrl")); // 回调地址
		sign.append("&fee_type=").append("CNY"); // 币种类型
		sign.append("&sp_client_ip=").append(SystemConfig.getValue("spbill_create_ip")); // IP地址
		sign.append("&sign=").append(sign_m); // 签名
		sign.append("&device_info=0"); // 设备类型
		sign.append("&body=").append(body+System.currentTimeMillis()); // 商品描述

		System.out.println("h5支付发送数据->" + sign.toString());

		JSONObject clentJson = HttpUtil.httpHengYunClentJson(url, sign.toString());

		if (clentJson.getInt("result_code") == 0)
			return clentJson;
		return new HashMap<String, Object>();
	}
//	public static String zhifupayH5(String orderId,BigDecimal amount,Integer userId,String goodsName) {
//
//		String url = "https://www.sys.cnjrbank.com/middle-stage/quicktransaction/appInterface";
//
//		Map<String,String> hashMap = new LinkedHashMap<String, String>() {
//			private static final long serialVersionUID = 1L;
//			{
//				put("P1_bizType", "AppRandomQuickPayHtml");
//				put("P2_orderId", orderId);
//				put("P3_customerNumber", "20191113150000XX9CD2ED68E2C34A67");
//				put("P4_orderAmount", amount.setScale(2, BigDecimal.ROUND_DOWN).toString());
//				put("P5_currency", "CNY");
//				put("P6_appType", "ALIPAY");
//				put("P7_userId", userId.toString());
//				put("P8_notifyUrl", SystemConfig.getValue("alipayH5zhifupaiNotify"));
//				put("P9_successToUrl", "");
//				put("P10_falseToUrl", "");
//				put("P11_goodsName", goodsName);
//				put("P12_goodsDetail", goodsName);
//			}
//		};
//
//		String signStr = "&";
//
//		for (Map.Entry<String, String> m : hashMap.entrySet()) {
//
//			signStr = signStr + m.getValue()+"&";
//		}
//
//		System.out.println("签名前的字符串->"+signStr+"201911135700XX511E84E83DC04121");
//
//		String sign = Md5Util.encodeByMD5ByLow(signStr+"201911135700XX511E84E83DC04121");
//
//		System.out.println("签名后的sign->"+sign);
//
//		hashMap.put("sign", sign);
//
//		String clentJson = HttpClientUtil.post(url, hashMap, "UTF-8");
//
//		JSONObject json = JSONObject.fromObject(clentJson);
//
//		System.out.println("支付渠道返回数据->"+json.toString());
//
//		if(json.getString("rt2_retCode").equals("0000")) {
//
//			System.out.println("进入了获取跳转url");
//
//			return json.getString("rt10_appPayUrl");
//		}
//
//		return "";
//	}

	/**
	 *  支付派支付
	 * @param orderId 订单id
	 * @param amount 金额
	 * @param userId 用户id
	 * @param goodsName 商城名字
	 * @return
	 */
	public static String zhifupayH5(String alipay_customerNumber,String alipayH5zhifupaiNotify,String orderId,BigDecimal amount,Integer userId,String goodsName,String appType) {
		String url = "https://www.sys.cnjrbank.com/middle-stage/quicktransaction/appInterface";

		Map<String,String> hashMap = new LinkedHashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("P1_bizType", "AppRandomQuickPayHtml");
				put("P2_orderId", orderId);
				// 支付派分批的商户号【商户ID】
				put("P3_customerNumber",alipay_customerNumber);
				put("P4_orderAmount", amount.setScale(2, BigDecimal.ROUND_DOWN).toString());
				put("P5_currency", "CNY");
				put("P6_appType", appType); //WXPAY:微信 ALIPAY：支付宝
				put("P7_userId", userId.toString());
				// 通知回调结果
				put("P8_notifyUrl", alipayH5zhifupaiNotify);
				put("P9_successToUrl", "");
				put("P10_falseToUrl", "");
				put("P11_goodsName", goodsName);
				put("P12_goodsDetail", goodsName);
			}
		};

		String signStr = "&";
		for (Map.Entry<String, String> m : hashMap.entrySet()) {
			signStr = signStr + m.getValue()+"&";
		}

		// 密钥
		String sign = Md5Util.encodeByMD5ByLow(signStr+SystemConfig.getValue("alipay_secret_key"));
		hashMap.put("sign", sign);
		String clentJson = HttpClientUtil.post(url, hashMap, "UTF-8");
		JSONObject json = JSONObject.fromObject(clentJson);
		if(json.getString("rt2_retCode").equals("0000")) {
			return json.getString("rt10_appPayUrl");
		}
		return null;
	}

	/**
	 *  支付派地址转换，换成app或微信支付
	 * @param orderId 订单id
	 * @param amount 金额
	 * @param userId 用户id
	 * @param goodsName 商城名字
	//	 * @param alipayH5zhifupaiType 支付类型 app/(微信)wx/(支付宝)zfb
	 * @return
	 */
	public static Map<String, Object> zhifupayUrl(String userName,String alipay_customerNumber,String alipayH5zhifupaiNotify,String orderId, BigDecimal amount, Integer userId, String goodsName,String appType){
		String appPayUrl = zhifupayH5(alipay_customerNumber,alipayH5zhifupaiNotify,orderId,amount,userId,goodsName,appType);
		if (appPayUrl != null){
			Map<String, Object> map = new HashMap<>();
			//支付派小程序支付原始原始ID【微信支付时必须】
			map.put("userName",SystemConfig.getValue("userName"));
			//请求地址
			map.put("path",appPayUrl);
			String alipayH5zhifupaiType ="wx";
			//截取字符串中必须参数
			String[] s = appPayUrl.split("\\?");
			s = s[1].split("&");
			String customerNumber = s[0].split("=")[1];
			String templateOrderId = s[1].split("=")[1];
			if ("wx".equals(alipayH5zhifupaiType)){
				map.put("path","pages/minipayment/minipayment?customerNumber="+customerNumber+"&templateOrderId="+templateOrderId);
				return map;
			}else if ("app".equals(alipayH5zhifupaiType)){
				map.put("path","pages/minipay/minipay?customerNumber="+customerNumber+"&templateOrderId="+templateOrderId);
				return map;
			}
		}
		throw new RuntimeException("提交支付信息异常");
	}


	public static void main(String[] args) {
//		System.err.println(zhifupayH5(System.currentTimeMillis()+"", new BigDecimal(1), 11, "测试","ALIPAY"));

	}

	
//	public static Map<String, Object> payOrder(Map<String, Object> remoteMap) {
//		Map<String, Object> paramMap = new HashMap<String, Object>();
//		paramMap.put("uid", UID);
//		paramMap.put("notify_url", NOTIFY_URL);
//		paramMap.put("return_url", RETURN_URL);
//		paramMap.put("pay_url", PAY_URL);
//		paramMap.putAll(remoteMap);
//		paramMap.put("key", getKey(paramMap));
//		logger.info("key参数查询="+JSONObject.fromObject(paramMap).toString());
//		return paramMap;
//	}
//
//	public static String getKey(Map<String, Object> remoteMap) {
//		String key = "";
//		if (null != remoteMap.get("goodsname")) {
//			key += remoteMap.get("goodsname");
//		}
//		if (null != remoteMap.get("istype")) {
//			key += remoteMap.get("istype");
//		}
//		if (null != remoteMap.get("notify_url")) {
//			key += remoteMap.get("notify_url");
//		}
//		if (null != remoteMap.get("orderid")) {
//			key += remoteMap.get("orderid");
//		}
//		if (null != remoteMap.get("orderuid")) {
//			key += remoteMap.get("orderuid");
//		}
//		if (null != remoteMap.get("price")) {
//			key += remoteMap.get("price");
//		}
//		if (null != remoteMap.get("return_url")) {
//			key += remoteMap.get("return_url");
//		}
//		key += TOKEN;
//		if (null != remoteMap.get("uid")) {
//			key += remoteMap.get("uid");
//		}
//		logger.info("支付KEY ： " + key);
//		return Md5Util.encodeByMD5ByLow(key);
//	}
//	
//	public static boolean checkPayKey(PaySaPi paySaPi) {
//		String key = "";
//		if (!StringUtils.isBlank(paySaPi.getOrderid())) {
//			logger.info("支付回来的订单号：" + paySaPi.getOrderid());
//			key += paySaPi.getOrderid();
//		}
//		if (!StringUtils.isBlank(paySaPi.getOrderuid())) {
//			logger.info("支付回来的支付记录的ID：" + paySaPi.getOrderuid());
//			key += paySaPi.getOrderuid();
//		}
//		if (!StringUtils.isBlank(paySaPi.getPaysapi_id())) {
//			logger.info("支付回来的平台订单号：" + paySaPi.getPaysapi_id());
//			key += paySaPi.getPaysapi_id();
//		}
//		if (!StringUtils.isBlank(paySaPi.getPrice())) {
//			logger.info("支付回来的价格：" + paySaPi.getPrice());
//			key += paySaPi.getPrice();
//		}
//		if (!StringUtils.isBlank(paySaPi.getRealprice())) {
//			logger.info("支付回来的真实价格：" + paySaPi.getRealprice());
//			key += paySaPi.getRealprice();
//		}
//		logger.info("支付回来的Key：" + paySaPi.getKey());
//		key += TOKEN;
//		logger.info("我们自己拼接的Key：" + Md5Util.encodeByMD5ByLow(key));
//		return paySaPi.getKey().equals(Md5Util.encodeByMD5ByLow(key));
//	}
//
//	public static String getOrderIdByUUId() {
//		int machineId = 1;// 最大支持1-9个集群机器部署
//		int hashCodeV = UUID.randomUUID().toString().hashCode();
//		if (hashCodeV < 0) {// 有可能是负数
//			hashCodeV = -hashCodeV;
//		}
//		// 0 代表前面补充0;d 代表参数为正数型
//		return machineId + String.format("%01d", hashCodeV);
//	}

}
